class MealsListData {
  MealsListData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = '',
    this.endColor = '',
    this.estado,
    this.valoracion = 0,
  });

  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  List<String> estado;
  int valoracion;

  static List<MealsListData> tabIconsList = <MealsListData>[
    MealsListData(
      imagePath: 'assets/fitness_app/iconoapp.png',
      titleTxt: 'Biux',
      valoracion: 50,
      estado: <String>['Completado,', 'App movil,', ],
      startColor: '#FA7D82',
      endColor: '#FFB295',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/devshouse.png',
      titleTxt: 'Biux',
      valoracion: 80,
      estado: <String>['Completado,', 'App movil,', ],
      startColor: '#3A6332',
      endColor: '#56AA30',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/Ibacrea1.png',
      titleTxt: 'Biux',
      valoracion: 100,
      estado: <String>['Completado,', 'App movil,', ],
      startColor: '#535465',
      endColor: '#0980BB',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/iconoapp.png',
      titleTxt: 'Biux',
      valoracion: 0,
     estado: <String>['Completado,', 'App movil,', ],
      startColor: '#6F72CA',
      endColor: '#1E1466',
    ),
  ];
}
